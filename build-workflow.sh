#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FUNC_NAME="${1}"

FUNC_DIR="${THIS_DIR}/${FUNC_NAME}"
BUILD_DIR="${THIS_DIR}/build/${FUNC_NAME}"

rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"

if [[ -d "${FUNC_DIR}/frecklets" ]]; then
   cp -r "${FUNC_DIR}/frecklets" "${BUILD_DIR}/frecklets"
fi

cp -r "${FUNC_DIR}/function" "${BUILD_DIR}/function"

cp "${THIS_DIR}/template/retailiate/Dockerfile" "${BUILD_DIR}/Dockerfile"
cp "${THIS_DIR}/template/retailiate/of-watchdog" "${BUILD_DIR}/of-watchdog"
cp "${THIS_DIR}/template/retailiate/requirements.txt" "${BUILD_DIR}/requirements.txt"
cp "${THIS_DIR}/template/retailiate/index.py" "${BUILD_DIR}/index.py"

touch "${BUILD_DIR}/__init__.py"

cd "${BUILD_DIR}"

docker pull registry.gitlab.com/retailiate/retailiate-core:dev

docker build -t "registry.gitlab.com/retailiate/workflows/${FUNC_NAME}:latest" .
docker push registry.gitlab.com/retailiate/workflows/${FUNC_NAME}:latest

cd "${THIS_DIR}"
faas-cli deploy -f "${FUNC_NAME}.yml"
